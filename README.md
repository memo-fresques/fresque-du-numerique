# Fresque du Numérique

Ce projet contient toutes les données pour générer le mémo de la [Fresque du Numérique](fresquedunumerique.org/) à partir du logiciel [memo viewer](https://framagit.org/memo-fresques/memo-viewer).


## Licence

Le mémo s'appuie sur le logiciel [Memo viewer](https://framagit.org/memo-fresques/memo-viewer) sous licence libre [GPLv3](https://framagit.org/memo-fresques/memo-viewer/-/blob/main/LICENSE).

Toutes les informations sur les cartes sont soumises à la license d'utilisation de la Fresque du numérique :

## License d'utilisation non-commerciale

Cette licence s'applique :
* losque les animateur·ices sont bénévoles ;
* et que la participation est à faible prix ou gratuite ;
* et que l'atelier a lieu dans un cadre privé, associatif ou dans l'enseignement formation initiale (étudiant·e·s et enseignant·e·s, y compris pendant le temps de travail de l’enseignant·e).

Cela exclut l’utilisation en prestation et en interne au sein d'une entreprise ou organisation publique (rémunérée ou non).

La licence appliquée est la Creative Commons BY-NC-ND, ce qui signifie :
* BY=attribution: Vous devez créditer l'œuvre. Concrètement, celas ignifie qu’en faisant usage de la
Fresque du Numérique, les auteurs doivent être cités. Cela signifie également que sans autorisation
des auteurs, il est interdit de reproduire les supports de la Fresque du Numérique en enlevant les
références aux auteurs, ou d’y ajouter un logo ou l’identification d’une association,d’une collectivité, d’une agence publique ou d’une entreprise.
* ND = pas de modifications : Il n’est pas possible de modifier lecontenu des cartes ou bien de
rajouter des cartes. Toute adaptation de l’atelier doit être explicitement validée par les auteurs.
* NC = pas d’utilisation commerciale : Il n’est pas possible d’utiliser librement la Fresque du
Numérique dans un cadre commercial. Une autre licence s’applique (voir le point suivant).

## Développement

* Récupérer le code source de ce repository :
```
git clone https://framagit.org/memo-fresques/fresque-du-numerique
```
* Récupérer le code source de memo-viewer et le rendre accessible dans un sous-dossier `memo-viewer`
    * Solution 1 : cloner le repo en local
    ```
    cd fresque-du-numerique
    git clone https://framagit.org/memo-fresques/memo-viewer.git memo-viewer
    ```
    * Solution 2 : créer un lien symbolique vers `memo-viewer`
    ```
    ln -s <path to memo-viewer> <path-to-fresque-du-numerique>/memo-viewer
    ```

## Générer les cartes et les images

Utiliser le script `scripts/generate_cards.py` pour générer toutes les images des cartes à partir de fichiers pdfs (un par langue). Les images sont volontairement exclues de ce repository Git afin de l'alléger.
Les pdf avec les différentes cartes peuvent être obtenues dans "l'espace animateur·ice" de l'association.

1. Créer un dossier `pdf-cards` et y ajouter le fichier fr-FR.pdf avec les cartes pdf français au complet.
2. Appeler le script `python3 scripts/generate_cards.py pdf-cards`

