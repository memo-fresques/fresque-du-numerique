---
title: Allonger la durée de garantie légale
backDescription: >-
  Cela permet d’allonger la durée de vie des équipements, et donc de réduire la
  quantité d'équipements fabriqués. Cela a de l'intérêt à condition que la
  réparation soit privilégiée sur l'échange à neuf pendant la période de
  garantie. Une durée de garantie légale réaliste pourrait être de 10 ans.
---

Conclusion principale du Rapport des Amis de la Terre sur le problème des DEEE : "La priorité doit être donnée à l’allongement de la durée de vie des produits et ceci passe par l’extension de la durée de la garantie légale de 2 à 10 ans"

Action également recommandée par l'association HOP (Halte à l'Obsolescence programmée).
