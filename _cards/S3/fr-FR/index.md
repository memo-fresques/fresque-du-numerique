---
title: Redonner vie à notre matériel inutilisé
backDescription: >-
  Donner ou revendre nos équipements fonctionnels inutilisés : ils seront bien
  plus utiles dans d'autres mains que dans un placard ! Pour le matériel non
  réutilisable, HS et/ou obsolète, améliorer le taux de collecte et le taux de
  recyclage.
---

Pour le matériel non réutilisable, HS et / ou obsolète, attention à ce qu’il soit bien pris en charge par des circuits de collecte réglementaires adaptés. Une télévision laissée dans la rue d’une grande ville finira très probablement dans un circuit illégal puis une décharge sauvage.
