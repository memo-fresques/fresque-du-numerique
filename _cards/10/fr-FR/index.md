---
backDescription: >-
  Il s'agit de la réduction de la durée de vie d’un bien par des facteurs
  techniques :

  - Obsolescence matérielle : fragile, difficile à réparer, pièces détachées
  chères…

  - Obsolescence logicielle : incompatibilités, ralentissements, durée limitée
  du support…


  Dans certains cas, si la réduction de la durée de vie est visée dès la
  conception du produit, l’obsolescence est programmée.
title: Obsolescence technique
---

La durée de vie d'un ordinateur a été divisée par près de 3 depuis 1985, passant de 11 à 4 ans. La durée de vie estimée d'un smartphone est autour de 2 ans.

On parle souvent d'obsolescence technique ou bien d'obsolescence matérielle.

Point de MIR parle aussi "d'obsolescence par usage inapproprié" = méconnaissance du produit (matériel et logiciel) conduisant à cesser son utilisation plus vite.

En France, la notion juridique d’obsolescence programmée a été utilisée pour porter plainte contre des fabricants d'imprimante en 2017. Par ailleurs Apple a finalement payé une amende pour “défaut de communication” pour “l’affaire des batteries iPhone” début 2020.

Pour aller plus loin, voir la section “Obsolescences” dans la partie approfondissement de ce guide.
