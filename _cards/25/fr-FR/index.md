---
backDescription: >-
  Devenus inutilisés par les mécanismes de l'obsolescence technique et
  psychologique, beaucoup d'équipements encore fonctionnels restent stockés dans
  des tiroirs… avant souvent d’être jetés des années plus tard.


  On estime que plusieurs milliards de téléphones mobiles sont inutilisés dans
  les foyers à travers le monde.
title: Non utilisation du matériel encore fonctionnel
---

_Source  : GSMA, étude ["Estimating the number of dormant phones worldwide"](https://www.gsma.com/betterfuture/wp-content/uploads/2023/06/Research-Methodology-2023.pdf), 2023_


Il y a entre 54 et 113 millions de téléphones "dormants" en France selon le rapport Ademe “[Équipements électriques et électroniques, données 2018](https://www.ademe.fr/sites/default/files/assets/documents/registre-deee-donnees-2018-rapport.pdf)”, p. 37, repris dans un rapport du Sénat, pour plus de 2/3 encore fonctionnels (gardés essentiellement comme solution de rechange, pour soi ou pour ses proches). Le chiffre des autres équipements numériques "dormants" (ordinateurs, objets connectés...) est fort probablement élevé aussi, mais nous n'avons pas trouvé d'étude ou rapport abordant cela.
