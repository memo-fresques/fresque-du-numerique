---
title: Limiter notre quantité d'équipements
backDescription: >-
  (1) Questionner notre besoin d'achat numérique (2) Éviter le suréquipement (3)
  Se renseigner sur la robustesse et l’indice de réparabilité pour acheter du
  matériel plus durable
---

Nous pouvons avoir besoin de quelques équipements clés, mais avons-nous vraiment besoin de nombreux équipements renouvelés fréquemment ? Avons-nous besoin de grille-pains connectés ? De bouteilles d’eau connectées ? (2 vrais exemples ! Voilà le [grille-pain](https://www.europe1.fr/technologies/le-grille-pain-connecte-lindispensable-du-ces-2943164) et [la bouteille d’eau](https://www.kickstarter.com/projects/582920317/hidrateme-smart-water-bottle)).

Des solutions techniques existent aussi pour permettre un usage du même ordinateur ou smartphone en pro + perso, tout en garantissant la sécurité et distinction privé / pro.
