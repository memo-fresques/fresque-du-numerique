---
backDescription: >-
  Le matériel numérique est composé principalement de métaux, il y en a plus de
  50 dans un équipement : aluminium, cuivre, or, argent, platine, cobalt,
  lithium, tantale, indium...

  Ces métaux sont présents en faible concentration dans des minerais extraits de
  la croûte terrestre.
title: Stress hydrique
---

_Source : rapport de France Stratégie ["La consommation de métaux du numérique : un secteur loin
d’être dématérialisé"](https://www.strategie.gouv.fr/publications/consommation-de-metaux-numerique-un-secteur-loin-detre-dematerialise), 2020, p.24 à 26_

A noter que le coût énergétique et environnemental très élevé de la désalinisation de l’eau,
envisagée pour alimenter certaines exploitations minières, n'en fait pas une solution durable.
Par exemple : l'extraction et raffinage du cuivre au Chili nécessite beaucoup d’eau douce dans un
endroit très sec (désert d’Atacama). Ceci nécessite de faire venir de l’eau de mer par tuyau, et donc
des usines de désalinisation (alimentées en électricité par des centrales à charbon...).
