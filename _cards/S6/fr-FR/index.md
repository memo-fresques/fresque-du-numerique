---
title: Faire preuve de sobriété numérique
backDescription: >-
  Pour éviter les multiples effets rebonds, le moyen le plus efficace est de
  modérer nos achats de matériel et nos usages numériques.
---

_cf._ la carte 35 “Effet rebond” : les gains d’efficience et d’efficacité ne sont pas suffisants pour faire baisser l’empreinte environnementale, cela demande avant tout de la sobriété, donc une action volontaire de réduction.
