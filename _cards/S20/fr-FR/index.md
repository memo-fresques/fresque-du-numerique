---
title: Contribuer aux choix collectifs
backDescription: >-
  Aider les avancées collectives : - Voter et exprimer son avis auprès de ses
  élus, députés, sénateurs, maires... - S’organiser entre citoyens pour faire
  pression sur les décideurs - Pousser ces sujets en interne au sein de son
  entreprise ou organisation
---

Nombre de participant·e·s se sentent impuissants ou dépassés face aux évolutions qui ne peuvent se faire que collectivement, alors que nous pouvons tous influer un petit peu d’une manière ou d’une autre sur les choix collectifs.
