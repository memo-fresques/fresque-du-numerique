---
title: Éco-concevoir le matériel numérique
backDescription: >-
  - Côté fabricant, développer du matériel robuste, modulaire, facilement
  réparable, avec des pièces détachées disponibles à prix accessible, et avec
  une empreinte réduite sur toute la chaîne de fabrication - Côté utilisateur,
  privilégier du matériel avec un haut niveau de durabilité et de réparabilité,
  pour qu’il vive longtemps et pour faire pression sur les fabricants
---

En France un indice de réparabilité est entré en vigueur en 2021. Pour plus d’information voir cet [article complet de The Conversation](https://theconversation.com/dans-la-fabrique-de-lindice-de-reparabilite-en-vigueur-depuis-janvier-2021-155536).
