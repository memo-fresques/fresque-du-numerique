---
title: Déchets électroniques
backDescription: >-
  En fin de vie, le matériel numérique que l'on utilise devient un "déchet
  d'équipement électriques et électroniques" (DEEE). Cela est accéléré par
  l'obsolescence technique et psychologique. Au niveau mondial, 17% de ces
  déchets sont collectés en vue d'un recyclage. Ce qui n'est pas recyclé est
  enfoui, incinéré, ou géré par des circuits illégaux.
---

_Source : rapport "[The Global E-waste Monitor 2020](https://www.itu.int/en/ITU-D/Environment/Documents/Toolbox/GEM_2020_def.pdf)", p. 23._

Environ 29 kg/hab de matériel électrique et électronique mis sur le marché en France en 2018 (1 929
000 tonnes). La moyenne européenne est de 17 kg/hab/an et la moyenne mondiale de 6 kg/hab/an.

En France en 2018 environ 40% du total DEEE (64% pour les DEEE informatiques) ont été collectés. Les
DEEE informatiques représentent environ 10% des DEEE ([Étude INSEE “Impacts environnementaux du
numérique”](https://www.insee.fr/fr/statistiques/4238589?sommaire=4238635#consulter), 2019, chiffres issus de la figure 3).

Le taux de collecte des DEEE en Europe est d'environ 40%, et dans le monde d'environ 17%.
Pour plus d’infos, voir zoom sur les déchets dans la partie approfondissement de ce guide.
