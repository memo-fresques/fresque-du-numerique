---
title: Dérèglement climatique
backDescription: >-
  Les gaz à effet de serre issus des activités humaines créent un effet de serre
  additionnel, ce qui entraîne une augmentation des températures moyennes, et
  accroît ainsi le nombre et l'intensité d'événements climatiques extrêmes :
  canicules, sécheresses, innondations, cyclones, incendies… Pour approfondir le
  sujet, nous vous conseillons "La Fresque du Climat"
---

_Source : rapport du GIEC (IPCC) AR6 “[Summary for Policymakers](https://www.ipcc.ch/report/ar6/wg1/downloads/report/IPCC_AR6_WGI_SPM_final.pdf)”, 2021_

Pour pouvoir rentrer plus en finesse dans les explications sur le dérèglement climatique, vous pouvez participer à la Fresque du Climat et approfondir le sujet, avec par exemple la correction interactive en ligne : https://memo.climatefresk.org/ et les résumés aux décideurs et / ou résumés techniques du dernier rapport du GIEC (AR6 sorti en 2022).
