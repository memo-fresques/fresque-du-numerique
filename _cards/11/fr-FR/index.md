---
title: Sac à dos écologique
backDescription: >-
  Cette carte est un focus sur l'ensemble de la partie fabrication. Le sac à dos
  écologique comptabilise toutes les ressources naturelles mobilisées depuis
  l'extraction des matières premières jusqu'à la fabrication du produit fini.
  Par exemple, la fabrication d'un ordinateur de 2 kg nécessite de mobiliser 800
  kg de ressources.
---

_Source : rapport de l'ADEME "[Modélisation et évaluation du poids carbone de produits de consommation et biens d'équipements](https://www.ademe.fr/sites/default/files/assets/documents/acv-biens-equipements-201809-rapport.pdf)", 2018, p. 25_

Dans la correction, cette carte reste flottante au-dessus de la partie amont du cycle de vie (fabrication et extraction), car elle ne respecte pas la logique de lien d’implication. Toutefois si les participant·e·s la relie aux cartes fabrication ou extraction et raffinage cela reste tout à fait acceptable.

Le sac à dos écologique compte les ressources mobilisées (déplacées ou utilisées) pour **l’amont** : depuis l’extraction des matières premières jusqu’à la fabrication du produit fini, pour les ressources
abiotiques (minéraux, pétrole, charbon...) et les ressources biotiques (bois, produits agricoles...).

C’est l'appellation vulgarisée de la notion de MIPS (Material Input Per Service unit), et une notion
popularisée en France par l’ADEME. Chercher dans son moteur de recherche “sac à dos écologique
ADEME”, sinon on tombe sur des sac à dos en lin bio ! :-)

Sur l'ordinateur environ les 3/4 des 800kg sont liés au cuivre, à l'or et au charbon.

Le sac à dos écologique d'un smartphone est autour de 200kg (et non 70kg comme indiquent de
vieux chiffres souvent repris dans des articles de presse).

Exemple concret de sac à dos : l'extraction et raffinage du cuivre au Chili, qui nécessite de l’eau dans
un endroit très sec, donc qui nécessite des canaux, et donc des usines de désalinisation alimentées
en électricité par des centrales à charbon.

Les chiffres sur l’empreinte eau sont incertains : Une [étude ADEME 2017](http://multimedia.ademe.fr/telechargements/Eco-gestes-informatiques-au-quotidien-010048-extrait.pdf) (p. 19/21) mentionne environ
20 kg de produits chimiques et 1500 litres d'eau. Mais cela reprend une [étude ancienne de 2003](https://archive.unu.edu/zef/publications-d/flyer.pdf).

Le sac à dos matière est souvent calculé séparément du sac à dos eau et du sac à dos air.

L'énergie grise est un concept similaire mais focalisé sur l'énergie nécessaire à fabriquer un équipement, et par extension sur l’empreinte carbone.
