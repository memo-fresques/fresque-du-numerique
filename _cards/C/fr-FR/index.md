---
backDescription: >-
  Le numérique nous permet de réaliser beaucoup de calculs qui nous seraient
  sinon impossibles, d'une simple requête sur un moteur de recherche aux travaux
  scientifiques dans de nombreux domaines.
title: Calculer
youtubeCode: 'http://www.tv4planet.tv/emissions/empreintes-digitales/1'
---

Voir [carte A](/card/A).
