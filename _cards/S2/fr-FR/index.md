---
backDescription: >-
  Cela permet d’allonger la durée de vie des équipements. Le matériel d'occasion
  peut être reconditionné par un professionnel et revendu avec une garantie.


  Par rapport à un équipement neuf, un équipement reconditionné représente une
  baisse d'environ 80% sur tous les impacts environnementaux (climat, sac à dos
  écologique, déchets, eau...).
title: Acheter d'occasion / reconditionné
---
_Source: ADEME, étude ["Évaluation de l'impact environnemental d'un ensemble de produits reconditionnés"](https://librairie.ademe.fr/dechets-economie-circulaire/5241-evaluation-de-l-impact-environnemental-d-un-ensemble-de-produits-reconditionnes.html), 2022_

Le matériel reconditionné par un pro à l’avantage d’être revendu avec une garantie d’au moins 6 mois. Des associations comme Ecodair, des entreprises comme Backmarket ou Recommerce contribuent à faciliter et faire grandir la pratique de l’achat / vente de matériel de seconde main reconditionné. Sans l’aspect reconditionnement, ces achats / ventes peuvent aussi se faire dans ses réseaux personnels ou via des plateformes de vente d’occasion généralistes.
