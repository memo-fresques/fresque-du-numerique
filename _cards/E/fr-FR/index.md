---
backDescription: >-
  La numérisation permet d’optimiser et d’accélérer tous les secteurs d’activité
  : industrie, agriculture, énergie, transport, logistique, bâtiment, santé,
  services publics, finance, sécurité…
title: Se divertir
---

Voir [carte A](/card/A).
