---
backDescription: >-
  Numériser ne permet généralement pas de réduire les impacts environnementaux
  au global. Pour éviter les multiples effets rebonds et réduire les impacts, le
  plus efficace est de modérer nos usages et de remettre le numérique à sa place
  par rapport au reste de nos vies.

  Tout n’a pas besoin d’être numérique : pour répondre à un besoin on peut aussi
  penser low-tech, simple et accessible.
title: Dé-numériser et être dans la sobriété
---

Pour en apprendre plus sur la philosophie Low-tech, RDV par exemple sur [le site de l’association Low Tech Lab](https://lowtechlab.org/fr).

Si l’on repense aux meilleurs moments que l’on passe dans sa semaine ou dans sa vie, ceux-ci sont rarement sur son smartphone ou derrière un écran
