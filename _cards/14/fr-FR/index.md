---
title: Répartition du trafic internet
backDescription: >-
  Ceci est un focus sur la carte "Internet et réseaux". 80% des flux mondiaux de
  donnnées internet sont des flux vidéo : vidéo à la demande, sites de partage
  vidéo, porno, vidéo de réseaux sociaux, TV, visioconférence,
  vidéosurveillance…
---

_Sources :_

- _Rapport “[Cisco VNI Global IP Traffic Forecast 2017-2022](https://cyrekdigital.com/uploads/content/files/white-paper-c11-741490.pdf)”, 2019, p. 13_
- _Rapport The Shift Project “[L’insoutenable usage de la vidéo en ligne](https://theshiftproject.org/wp-content/uploads/2019/07/R%C3%A9sum%C3%A9-aux-d%C3%A9cideurs_FR_Linsoutenable-usage-de-la-vid%C3%A9o-en-ligne.pdf)”, 2019_

En France, Netflix occupe environ un quart de la bande passante (_cf._ [cet article de presse](https://www.ouest-france.fr/medias/netflix/netflix-represente-pres-d-un-quart-du-trafic-de-l-internet-francais-6882613#:~:text=La%20plateforme%20repr%C3%A9sentait%20l'ann%C3%A9e,%C3%89tat%20d'internet%20en%20France.) par
exemple, source primaire : Rapport ARCEP 2019).
