---
title: Tensions géopolitiques
backDescription: >-
  Face à une demande corissante, des pénuries de certaines ressources
  nécessaires au numérique peuvent provoquer des arbitrages entre usages,
  filières industrielles ou clients. Cela peut engendrer des tensions
  géopolitiques entre pays exportateurs et importateurs, voire déboucher sur des
  conflits.
---

Beaucoup de métaux rares sont sensibles car extraits par un seul ou une poignée de pays, pour des raisons géologiques et/ou économiques. Les métaux,de plus en plus stratégiques,sont en quelque sorte en train de devenir le pétrole du 21ème siècle en termes d’enjeux géopolitiques et de conflits.

Ce "carburant" de la transition numériqueet énergétique a déjà causé des tensions. Par exemple, la Chine a mis un embargo (officieux)sur l'exportation de métaux rares vers le Japon en septembre 2010 pendant 6 mois (source "La guerre des métaux rares" de Guillaume Pitron).
