---
backDescription: |-
  Tout commence par l’utilisation d’équipements numériques...
  Plus de 4 milliards de smartphones sont utilisés dans le monde.
  Ces équipements numériques doivent être alimentés en électricité.
title: Utiliser un smartphone
---

_Source : [Gartner (April 2021)](https://www.gartner.com/en/newsroom/press-releases/2021-04-01-gartner-forecasts-global-devices-installed-base-to-reach-6-2-billion-units-in-2021)_

Ordinateur et smartphone sont les 2 équipements auxquels le public pense le plus souvent lorsque
nous parlons de numérique, mais loin d'être les seuls
