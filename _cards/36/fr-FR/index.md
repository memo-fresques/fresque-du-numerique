---
backDescription: >-
  D'autres activités et secteurs ont un impact environnemental plus important
  que le numérique : par exemple la voiture, l’avion, la viande, le chauffage,
  ou la production de biens tels que vêtements et électroménager.


  L’impact du numérique n'est donc pas à considérer isolément du reste, il est
  important de prendre en compte de manière globale et systémique tous les
  impacts environnementaux de nos activités humaines.
title: Au-delà du numérique
---

