---
title: Sortir de l’obsolescence logicielle
backDescription: >-
  - Dissocier mises à jour correctives et évolutives - Imposer la réversibilité
  des mises à jour - Ouvrir le code des logiciels après la fin de leur support
  technique - Privilégier des logiciels libres / légers / avec un support longue
  durée - Nettoyer le système régulièrement et désinstaller les logiciels
  inutilisés Si nécessaire, vous trouverez certainement de l'aide dans votre
  entourage ou via des initiatives associatives.
---

Un autre exemple : installer Linux sur un vieil ordinateur qui commence à être trop lent peut lui donner une seconde jeunesse !
