---
backDescription: >-
  Le numérique facilite les échanges commerciaux, simplifie l'accès à la
  consommation et augmente les flux de marchandise.
title: Commercer
youtubeCode: 'http://www.tv4planet.tv/emissions/empreintes-digitales/1'
---

Voir [carte A](/card/A).
