---
title: Questionner les nouveaux usages
backDescription: >-
  Réserver la technologie aux cas où elle améliore le bien-être humain, en
  mettant en balance les bénéfices humains et environnementaux espérés avec les
  impacts négatifs. Par exemple, un déploiement 5G engendre des effets rebonds
  importants et des impacts environnementaux négatifs face à des bénéfices
  mitigés.
---

_Source : rapport du Haut Conseil pour le Climat "[Maîtriser l'impact carbone de la 5G](https://www.hautconseilclimat.fr/wp-content/uploads/2020/12/haut-conseil-pour-le-climat_rapport-5g.pdf)", 2020_
