---
backDescription: >-
  En cas de panne ou de casse : réparer ! 

  Cela peut être fait par un professionnel, dans un ""Repair Café"", ou
  soi-même.


  Au niveau collectif, cela peut passer par créer un fonds de réparation abondé
  par les fabricants, imposer la disponibilité des pièces détachées, ou
  sanctionner l’irréparabilité intentionnelle.
title: Réparer notre matériel
---

La réparation est souvent perçue comme une action non-rentable, mais bien des réparations valent le coup financièrement. Par exemple, faire remplacer une batterie de smartphone défectueuse par une neuve via un réparateur pro coûte généralement entre 50 et 80€.
