---
title: Sensibiliser autour de nous
backDescription: >-
  La connaissance approfondie du problème et des solutions potentielles est un
  moteur d'action pour emmener avec nous collaborateurs, partenaires, clients,
  représentants politiques, entourage, enfants ou élèves dans une transition
  vers un numérique plus durable.
---

Par exemple avec la Fresque du numérique ! Mais pas que : toute information transmise au cours d’une conversation contribue à faire évoluer les mentalités et les niveaux de connaissances.

Montrer l’exemple est aussi la meilleure façon de convaincre.
