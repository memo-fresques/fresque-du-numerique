---
backDescription: >-
  Internet est un réseau mondial de matériel informatique interconnecté : d'un
  côté les équipements numériques utilisateurs, et de l'autre infrastructures
  réseau et data centers.

  66% de l'humanité utilise internet, et le volume de données échangées augmente
  fortement au fil du temps.
title: Internet et réseaux
---
_Source :_
- _Recto : [Banque Mondiale, World Development Report 2021](https://wdr2021.worldbank.org/stories/crossing-borders/#:~:text=How%20big%20are%20global%20data,GB%20per%20person%20per%20day) et [ITU (United Nations specialized agency for ICT), Individuals using Internet, 2022](https://www.itu.int/itu-d/reports/statistics/2022/11/24/ff22-international-bandwidth-usage/)_

- _Verso : [ITU (United Nations specialized agency for ICT), Individuals using Internet, 2022](https://datahub.itu.int/data/?e=701&i=11624)_

On voit bien ici la dynamique exponentielle du déploiement du numérique : le trafic internet mondial a été multiplié par 17 en 10 ans (2010-2020).
Un chiffre complémentaire utile : au niveau français, ce sont 88% des plus de 12 ans qui utilisent internet (source : Enquête CREDOC "Baromètre du numérique, 2019", p. 9), donc 12% de la population ado et adulte est exclue du numérique.
