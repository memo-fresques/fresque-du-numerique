---
backDescription: >-
  La mutualisation permet de maximiser l’utilisation de matériel et d’en réduire
  le nombre. A-t-on besoin :

  - de 50 box internet dans les 50 appartements d'une copropriété ?

  - de 4 réseaux télécom différents ?

  - de téléphones ou ordinateurs pro et perso ?

  - de plusieurs écrans par personne ?
title: Mutualiser
---

Les 4 réseaux mobiles, c’est similaire au fait d'avoir un seul réseau électrique versus 4 réseaux électriques parallèles.

Le fait de n’avoir qu’une seule box dans une copropriété semble souvent totalement irréaliste aux participant·e·s. Pourtant c’est possible et ça existe depuis plusieurs années ! Voir par exemple l’initiative [FON](https://fon.com/), ou encore FreeWifi.
