---
backDescription: |-
  - Espacer les périodes de renouvellement du matériel
  - Considérer la durabilité des achats matériel ET logiciel
  - Opter pour des contrats de réparation et maintenance longue durée
title: Ajuster la politique d'achats
---

Espacer les périodes de renouvellement du matériel est souvent la solution la plus impactante pour une entreprise.
