---
title: Protéger et prendre soin de notre matériel
backDescription: >-
  Cela permet d’allonger la durée de vie des équipements. - Utiliser une housse,
  coque, protection d’ écran, sac de transport... - Éviter les températures et
  les niveaux de charge extrêmes Un objectif peut être de faire durer notre
  smartphone 6 ans et notre ordinateur 12 ans : 3 fois les moyennes actuelles.
---

_Source : rapport de l'ADEME "[Étude sur la durée de vie des équipements électriques et électroniques](https://www.ademe.fr/etude-duree-vie-equipements-electriques-electroniques)" 2012, p.43_

C’est une action souvent considérée comme évidente et pourtant nous protégeons souvent peu notre matériel, et une quantité énorme d’équipements se retrouvent prématurément endommagés. Les écrans de smartphone cassés en sont un très bon exemple.
