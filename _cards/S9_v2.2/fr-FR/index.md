---
backDescription: >-
  Prendre en compte de manière globale et systémique tous nos impacts
  environnementaux :

  - Agir aussi sur d’autres leviers que le numérique (transports, achats,
  alimentation, bâtiment...)

  - Questionner les modèles économiques de nos organisations (économie
  régénérative, économie de la fonctionnalité...)
title: Agir et penser global
---

