---
title: Data centers
backDescription: >-
  Pour fonctionner, internet et les réseaux ont besoin de centres de stockage et
  de traitement des données, appelés data centers. Ils sont composés de
  serveurs, dont quelques centaines de millions sont en service dans le monde.
  Ils doivent être alimentés en électricité.
---

La carte cite le nombre de serveurs physiques actuellement utilisés : quelques centaines de millions. Ce chiffre est vague car il y a plusieurs estimations mais pas de source fiable pour cette donnée.
