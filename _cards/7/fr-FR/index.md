---
backDescription: >-
  Tout commence par l’utilisation d’équipements numériques...

  34 milliards sont utilisés dans le monde : au-delà des ordinateurs et
  smartphones, ce sont aussi des télévisions, objets connectés, consoles de jeux
  vidéo, tablettes, imprimantes, caméras de surveillance, écrans
  publicitaires... 

  Ce chiffre croît fortement chaque année.


  Ces équipements numériques doivent être alimentés en électricité.
title: Utiliser un équipement numérique
---

_Source : rapport de GreenIT.fr "[Empreinte environnementale du numérique mondial](https://www.greenit.fr/wp-content/uploads/2019/10/2019-10-GREENIT-etude_EENM-rapport-accessible.VF_.pdf)", 2019_

En nombre, plus de la moitié de ces équipements sont des objets connectés. Un habitant d'Europe occidentale possède en moyen 9 équipements électroniques en 2021 (source : [guide ADEME, "La face cachée du numérique](https://www.ademe.fr/sites/default/files/assets/documents/guide-pratique-face-cachee-numerique.pdf)" 2021, p.4)
