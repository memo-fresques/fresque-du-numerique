---
title: Fabrication des équipements numériques
backDescription: >-
  Extraction, fabrication des composants, assemblages, transport : des
  industries sont nécessaires pour fabriquer les équipements numériques que l'on
  utilise. La fabrication nécessite à la fois de l'énergie et des ressources,
  notamment des métaux. La fabrication des équipements numériques mobilise une
  grande part de l'énergie et l'essentiel des ressources naturelles utilisées
  par le numérique.
---

_Source : rapport de GreenIT.fr "[Empreinte environnementale du numérique mondial](https://www.greenit.fr/wp-content/uploads/2019/10/2019-10-GREENIT-etude_EENM-rapport-accessible.VF_.pdf)", 2019_

Dynamique actuelle : le nombre d’ordinateurs est en léger déclin, pour les smartphones la croissance a ralenti, et il y a une forte hausse des objets connectés.

Le matériel numérique est composé principalement de métaux, puis en moindre part, de plastiques, de verre et de céramiques.

Bien faire remarquer que les équipements numériques et leur fabrication sont des sujets énormes ! Plus de 75% du camembert Ressources.

Selon l’étude de ADEME & ARCEP\*, les terminaux utilisateur sont responsables de 65% à 90% des impacts environnementaux du numérique en analyse multicritères (selon l’indicateur environnemental considéré : GES, ressources, eau, pollutions...) et multi-étapes de cycle de vie.

\* source : "[Évaluation de l’impact environnemental du numérique en France](https://www.arcep.fr/uploads/tx_gspublication/etude-numerique-environnement-ademe-arcep-note-synthese_janv2022.pdf)", 2022
